// clearance model for a cherry MX style switch's mounting plate
// this is basically the minimal shape to mill out of a block to be able to house a switch in it

// model is centered on the top opening, and set at and below the 0 height

// switch width
// cherry browns need 14mm
// kailh box navy is ok with 13.9
top_open_x = 14;

// switch length (above the mounting clips)
top_open_y = 13.95;

// height from switch mounting plate lip to the housing bottom
plate_max_z = 5;
// thickness of mounting section of the plate
plate_mount_z = 1.5;

// clip hook clearance
clip_x = 4.5;
clip_y = 0.5;

// round corners for milling
mouse_ears_r = 1.5;
mouse_ears_off = mouse_ears_r * 0.7071;

module switch_plate_clearance(ears = !$preview, stab = false) {
	union()
	translate([0, 0, - plate_max_z / 2]) {
		// main hole
		cube([top_open_x, top_open_y, plate_max_z], center = true);

		// mouse ears on main hole
		if (ears) {
			translate([(top_open_x / 2) - mouse_ears_off, (top_open_y / 2) - mouse_ears_off, 0])
			cylinder(r = mouse_ears_r, h = plate_max_z, center = true);
			translate([- (top_open_x / 2) + mouse_ears_off, (top_open_y / 2) - mouse_ears_off, 0])
			cylinder(r = mouse_ears_r, h = plate_max_z, center = true);
			translate([- (top_open_x / 2) + mouse_ears_off, - (top_open_y / 2) + mouse_ears_off, 0])
			cylinder(r = mouse_ears_r, h = plate_max_z, center = true);
			translate([(top_open_x / 2) - mouse_ears_off, - (top_open_y / 2) + mouse_ears_off, 0])
			cylinder(r = mouse_ears_r, h = plate_max_z, center = true);
		};

		// clip clearance
		translate([0, 0, - plate_mount_z / 2]) {
			cube([clip_x, top_open_y + (clip_y * 2), plate_max_z - plate_mount_z], center = true);

			// mouse ears for clips
			if (ears) {
				translate([(clip_x / 2) - mouse_ears_off, (top_open_y / 2) - mouse_ears_off + clip_y, 0])
				cylinder(r = mouse_ears_r, h = plate_max_z - plate_mount_z, center = true);
				translate([- (clip_x / 2) + mouse_ears_off, - (top_open_y / 2) + mouse_ears_off - clip_y, 0])
				cylinder(r = mouse_ears_r, h = plate_max_z - plate_mount_z, center = true);
				translate([(clip_x / 2) - mouse_ears_off, - (top_open_y / 2) + mouse_ears_off - clip_y, 0])
				cylinder(r = mouse_ears_r, h = plate_max_z - plate_mount_z, center = true);
				translate([- (clip_x / 2) + mouse_ears_off, (top_open_y / 2) - mouse_ears_off + clip_y, 0])
				cylinder(r = mouse_ears_r, h = plate_max_z - plate_mount_z, center = true);
			};
		};
	};
};
