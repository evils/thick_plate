include <layout.scad>

$fn = $preview ? 10 : 128;

//plate_x = 288.5; // based on eyeballing with ruler
plate_x = 288.76; // based on caliper measurement around mount3 below
plate_y = 96.87; // widest caliper measurement
stock_plate_x = 285.5; // eyeballed with ruler
stock_plate_y = 95.52;
plate_z = 5;
// 4mm drill is slightly loose, 4.5mm is slightly larger than the case inside corner
corner_r = 2;

// mounting posts
mount_post_r = 3.9 / 2;
mount_post_z = 5; //height of posts and support bumps from case floor (excl pocket)
// posts 1, 2 & 5 measured from left and front sides
mount1_x = 24.8 + mount_post_r;
mount1_y = 65.85 + mount_post_r;
mount2_x = 128.3 + mount_post_r;
mount2_y = 47 + mount_post_r;

mount_screw_r = 1.95 / 2;
mount5_x = 5.79 - mount_post_r; // assuming half round post has the same radius
mount5_y = 38.3 + mount_screw_r;
mount5_y_compl = 56.68 + mount_screw_r;

// posts 3, 4 & 6 measured from right and front sides
mount3_post_d = 3.87;
mount3_x_compl = 189.84 + mount3_post_d / 2;

echo("calculated plate_x:", 95.05 + mount3_post_d / 2 + mount3_x_compl);

mount3_x = plate_x - 95.05 - mount3_post_d / 2;
mount3_y = 8.38 + mount_post_r;

echo("mount3_x error:", mount3_x - mount3_x_compl);

mount4_x = plate_x - 25.3 - mount_post_r;
mount4_y = mount1_y;

mount6_x = plate_x - 5.78 - mount_post_r; // assuming half round post has the same radius
mount6_y = 38.34 + mount_screw_r; // from front
mount6_y_compl = plate_y - 56.64 - mount_screw_r;

echo("mount1:", mount1_x, mount1_y);
echo("mount2:", mount2_x, mount2_y);
echo("mount3:", mount3_x, mount3_y);
echo("mount4:", mount4_x, mount4_y);
echo("mount5:", mount5_x, mount5_y);
echo("mount6:", mount6_x, mount6_y);

// for completeness
usb_x = 12.88;
usb_z = 5.95;
usb_r = 2; // radius in port opening, seems the same as the rest of the case
case_wall = 3.13;
usb_x_off = 13.37; // offset of port opening from inside of left wall
usb_x_off_compl = 16.8; // offset of port opening from outside of left wall
pocket_x = 154;
pocket_y = 45;
pocket_z = 2;
pocket_r = 5;
pocket_l_off_x = 59.5;
pocket_r_off_x = 74.9;
pocket_off_y = 51.85;
support_x = 3;
support_y = 10; // very approximate, approximately diagonal, with a 5mm radius?
support_chord = 5; // approx width of spherical section
// support bumps, center from nearest wall, eyeballed
support1_x = 70; // bottom left, from left wall
support2_x = 93.2; // top left, from left wall
support3_x = 91.7; // top right, from right wall
// case
case_wall_z = 11.4; // from case floor
case_x = 295.5; // eyeballed on ruler
case_y = 103.24; // quite consistent
case_z = 14.55; // down to 14.36 // case total height excluding feet
// switch/reset hole (chamfered on the bottom)
hole_x = 11.8;
hole_y = 12.8;
hole_r = 2;
hole_off_x = 25; // from inside of left wall
hole_off_y = 41; // from inside of front wall

// tilt feet
foot_r = 4.23 / 2; // radius of mounting hole (counter-sunk from top)
foot1_x = 11.4 + foot_r; // from case left
foot1_y = 13; //from case rear
foot2_x = 12.75 + foot_r; // from case right
foot2_y = 13; //from case rear
foot_d = 21.67;
foot_h = 10; // excluding rubber bumpon

screw_head_d = 3.5;
screw_head_z = 1.5;
// no clearance hole means having to remove all the switches to remove the plate
screw_head_clearance = 4.6; // 3 - 4.52mm should suffice

// actual thing

module blank_plate() {
	translate([corner_r, corner_r, -plate_z]) {
		minkowski() {
			cube([plate_x - corner_r, plate_y - corner_r, plate_z - 0.01]);
			cylinder(r = corner_r, h = 0.01);
		};
	};
};

module screw_access() {
	translate([0, 0, -0.1])
	cylinder(d = screw_head_clearance, h = plate_z + 0.2);
};

module screw_clearance(clearance = true) {
	translate([0, 0, -plate_z])
	cylinder(d = screw_head_d, h = screw_head_z);
	if (clearance) {
		translate([0, 0, -plate_z])
		screw_access();
	};
};

module mounted_plate() {
	difference() {
		blank_plate();

		translate([mount1_x, mount1_y, 0])
		screw_clearance();
		translate([mount2_x, mount2_y, 0])
		screw_clearance();
		translate([mount3_x, mount3_y, 0])
		screw_clearance();
		translate([mount4_x, mount4_y, 0])
		screw_clearance();
		translate([mount5_x, mount5_y, 0])
		screw_clearance();
		translate([mount6_x, mount6_y, 0])
		screw_clearance();
	};
};

module switched_mounted_plate() {
	difference() {
		mounted_plate();
		// TODO: confirm this translate
		translate([u/2 + (plate_x - stock_plate_x) / 2, u/2 + (plate_y - stock_plate_y) / 2, 0]){
			clearance_layout();
			iso_enter();
			iso_stabs();
		};
	};
};

translate([plate_x, 0, 0])
rotate([0, 180, 0])
switched_mounted_plate();
