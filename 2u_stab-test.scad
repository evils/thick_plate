include <switch_plate_clearance.scad>
include <stab_clearance.scad>

$fn = $preview ? 32 : 256;

translate([2 * -19.05, 0, 0])
rotate([180, 0, 0])
difference() {
	translate([0, 0, -4.98])
	cube([2 * 19.05, 25.4, 4.98]);
	translate([19.05, 25.4 / 2, 0]) {
		stab_clearance(units = 2, ears = false);
		switch_plate_clearance(ears = false);
	};
};
