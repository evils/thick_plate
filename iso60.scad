include <switch_plate_clearance.scad>

u = 19.05;

default = [
	[1,   1,1,1,1,1,1,1,1,1,1,1,1,2],
	[1.5 ,1,1,1,1,1,1,1,1,1,1,1,1],
	[1.75,1,1,1,1,1,1,1,1,1,1,1,1],
	[1.25,1,1,1,1,1,1,1,1,1,1,1,2.75],
	[1.25,1.25,1.25,6.25,1.25,1.25,1.25,1.25]
];

function add_up_to(v, n) =  v[n] + (n == 0 ? 0 : add_up_to(v, n - 1));
function col_x(v, n) = n == len(v) ? col_x(v, n - 1) : v[n] / 2 + (n == 0 ? 0 : add_up_to(v, n - 1));

module iso60(layout = default) {
	translate([-u / 2, 0, 0]) {
		for (i = [0:len(layout) - 1]) {
			translate([0, (4 - i) * u, 0])
			for (j = [0:len(layout[i]) - 1]) {
				translate([col_x(layout[i], j) * u, 0, 0])
				switch_plate_clearance();
			};
		};

		// ISO enter
		translate([add_up_to(layout[2], 12) * u + 1.25 * u / 2, 2.5 * u, 0])
		switch_plate_clearance();
	};
};
