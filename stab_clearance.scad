$fn = $preview ? 16 : 128;

u = 19.05;
plate_z = 5;
plate_mount_z = 1.5;

mill_r = 1;

foam_z = 0.4;
block_y = 11.5;
base_clip_z = 3.15 + foam_z;
//base_clip_z = plate_z - plate_mount_z; // same height as the switch clip

module stab_housing(ears = true) {

	//foam_z = 0.4;
	block_x = 7;
	//block_y = 11;
	block_z = 7.1 + foam_z;
	block_r = 1.5;
	base_screw_y = 4.3;
	base_screw_z = 2.8 + foam_z;
	base_screw_r = 1.5;
	base_clip_y = 5;
	//base_clip_z = 3.15 + foam_z;
	base_clip_r = 1;
	wire_gap_x = block_x - (2.2 * 2);
	wire_gap_y = 1.3;
	wire_gap_z = 1.2;


	// main body
	linear_extrude(height = block_z)
	minkowski() {
		square([block_x - block_r * 2, block_y - block_r * 2], center = true);
		circle(block_r);
	};

	// screw wing
	linear_extrude(height = base_screw_z)
	translate([0, (-block_y / 2) - (base_screw_y / 2) + block_r, 0])
	minkowski() {
		square([block_x - base_screw_r * 2, base_screw_y + block_r - base_screw_r], center = true);
		circle(base_screw_r);
	};

	// clip wing
	// cover the block radius on this side
//	translate([0, (block_y / 2) - (block_r / 2), base_clip_z / 2])
//	cube([block_x, block_r, base_clip_z], center = true);
//	// actual wing
//	linear_extrude(height = base_clip_z)
//	translate([0, (block_y / 2), 0])
//	minkowski() {
//		square_y = base_clip_y - (base_clip_r * 2);
//		translate([0, square_y - base_clip_r, 0])
//		square([block_x - base_clip_r * 2, base_clip_y - base_clip_r], center = true);
//		circle(base_clip_r);
//	};
	translate([0, (block_y / 2) + (base_clip_y / 2) - (block_r / 2), base_clip_z / 2])
	cube([block_x, base_clip_y + block_r, base_clip_z], center = true);
	// mouse ears
	if (ears == true) {
		ear_off = -1 / (mill_r * sqrt(2));
		translate([(block_x / 2) + ear_off, (block_y / 2) + base_clip_y + ear_off, 0])
		cylinder(r = mill_r, h = base_clip_z);
		translate([(-block_x / 2) - ear_off, (block_y / 2) + base_clip_y + ear_off, 0])
		cylinder(r = mill_r, h = base_clip_z);
	};

	// wire gap
	wire_gap_r = 1;
	translate([0, (block_y / 2), base_clip_z])
	linear_extrude(height = wire_gap_z)
	minkowski() {
		square([wire_gap_x - (wire_gap_r * 2), wire_gap_y - wire_gap_r], center = true);
		circle(wire_gap_r);
	};
};

module stab_clearance(units = 2, ears = true) {

	wire_d = 1.7;
	wire_from_block = 1.27;
	wire_from_floor = 0.8;
	wire_clearance_height = 2.6 + foam_z;

	spacing = (u / 4) + ((units - 1) * u);

	translate([0, 0, -5]) {
		// wire clearance
		sliver = 0.05; // sliver left between wire clearance and switch clearance
		translate([-(spacing / 2), block_y / 2 + wire_from_block - sliver, 0])
		cube([spacing, 2 + sliver, wire_clearance_height]);

		// housings on either end
		translate([-spacing / 2, 0, 0])
		stab_housing(ears = ears);
		translate([spacing / 2, 0, 0])
		stab_housing(ears = ears);
	};
};
