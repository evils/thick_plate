include <layout.scad>

$fn = $preview ? 32 : 256;

translate([0, 0, 5])
mirror([0, 0, 1])
difference() {
	cube([u * 3, u * 3, 5]);
	translate([u/2, -u * 1.5, 5])
	clearance_layout(layout = [[1,1,1],[1,1,1],[1,1,1]]);
};
