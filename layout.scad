include <switch_plate_clearance.scad>
include <stab_clearance.scad>

u = 19.05;

// does not include ISO enter
iso60 = [[1, 1,1,1,1,1,1,1,1,1,1,1,1,2],
	[1.5,1,1,1,1,1,1,1,1,1,1,1,1],
	[1.75,1,1,1,1,1,1,1,1,1,1,1,1],
	[1.25,1,1,1,1,1,1,1,1,1,1,1,2.75],
	[1.25,1.25,1.25,6.25,1.25,1.25,1.25,1.25]];

function add_up_to(v, n) =  v[n] + (n == 0 ? 0 : add_up_to(v, n - 1));
function col_x(v, n) = n == len(v) ? col_x(v, n - 1) : v[n] / 2 + (n == 0 ? 0 : add_up_to(v, n - 1));

// layout should be a 2D vec like 2_2 = [[1,1],[1,1]]
module clearance_layout(layout = iso60) {
	translate([-u / 2, 0, 0]) {
		for (i = [0:len(layout) - 1]) {
			translate([0, (4 - i) * u, 0])
			for (j = [0:len(layout[i]) - 1]) {
				translate([col_x(layout[i], j) * u, 0, 0])
				switch_plate_clearance();
			};
		};

	};
};

module iso_enter() {
	translate([-u / 2, 0, 0])
	translate([add_up_to(iso60[2], 12) * u + 1.25 * u / 2, 2.5 * u, 0])
	switch_plate_clearance();
};

module iso_stabs() {
	translate([-u / 2, 0, 0]) {
		// ISO enter
		translate([add_up_to(iso60[2], 12) * u + 1.25 * u / 2, 2.5 * u, 0])
		rotate([0, 0, 90])
		stab_clearance(2);
		// backspace
		translate([add_up_to(iso60[0], 12) * u + (iso60[0][13] * u) / 2, 4 * u, 0])
		rotate([0, 0, 180])
		stab_clearance(2);
		// right shift
		translate([add_up_to(iso60[3], 11) * u + (iso60[3][12] * u) / 2, 1 * u, 0])
		rotate([0, 0, 180])
		stab_clearance(2);
		// spacebar
		translate([add_up_to(iso60[4], 2) * u + (iso60[4][3] * u) / 2, 0 * u, 0])
		stab_clearance(6.25);
	};
};
