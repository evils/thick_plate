// for a olkb planck lo-pro milled case

include <layout.scad>

$fn = $preview ? 10 : 128;

plate_x = 137.17 + 137.17 - 41.43 ; // based on stacked caliper measurements
plate_y = 81.2; // widest caliper measurement
plate_z = 5; // based on switch heights
plate_mount_z = 1.6; // from stock plate
corner_r = 0.5; // almost sharp corner, finish by hand
wall = 3.2;
wall_corner_r = 15 / 2; // rough guess, may be less
screw_thread_clearance = 2.6; // from stock plate, for a 2mm screw

// layout
planck = [
	[1,1,1,1,1,1,1,1,1,1,1,1],
	[1,1,1,1,1,1,1,1,1,1,1,1],
	[1,1,1,1,1,1,1,1,1,1,1,1],
	[1,1,1,1,1, 2 ,1,1,1,1,1]
];

// mounting posts
mount_post_r = 4.5 / 2;
mount_post_z = 7.8; // height of posts and walls from case floor (excl pocket)

// measured from outside of case to center of mounting post (based on mounting post diameter)
// as the plate extends on top of the aluminium case
// and from bottom left
mount1_x = 23.7 - mount_post_r;
mount1_y = mount1_x;
mount2_x = mount1_x;
mount2_y = plate_y - mount1_x;
mount3_x = plate_x / 2;
mount3_y = plate_y / 2;
mount4_x = plate_x - mount1_x;
mount4_y = mount1_x;
mount5_x = mount4_x;
mount5_y = plate_y - mount1_x;

echo("mount1:", mount1_x, mount1_y);
echo("mount2:", mount2_x, mount2_y);
echo("mount3:", mount3_x, mount3_y);
echo("mount4:", mount4_x, mount4_y);
echo("mount5:", mount5_x, mount5_y);

// actual thing

module blank_plate() {
	translate([corner_r, corner_r, -plate_mount_z])
	linear_extrude(height = plate_mount_z)
	minkowski() {
		square([plate_x - corner_r, plate_y - corner_r]);
		circle(corner_r);
	};
	translate([plate_x / 2, plate_y / 2, - plate_z])
	linear_extrude(height = plate_z - plate_mount_z)
	minkowski() {
		square([plate_x - ((wall + wall_corner_r) * 2), plate_y - ((wall + wall_corner_r) * 2)], center = true);
		circle(wall_corner_r);
	};
};

module screw_clearance() {
	translate([0, 0, -plate_mount_z])
	cylinder(d = screw_thread_clearance, h = plate_mount_z);
	translate([0, 0, -plate_z])
	cylinder(r = mount_post_r + 0.25, h = plate_z - plate_mount_z);
};

module mounted_plate() {
	difference() {
		blank_plate();

		translate([mount1_x, mount1_y, 0])
		screw_clearance();
		translate([mount2_x, mount2_y, 0])
		screw_clearance();
		translate([mount3_x, mount3_y, 0])
		screw_clearance();
		translate([mount4_x, mount4_y, 0])
		screw_clearance();
		translate([mount5_x, mount5_y, 0])
		screw_clearance();
	};
};

module switched_mounted_plate() {
	difference() {
		mounted_plate();
		// TODO: confirm this translate
		translate([11.7, -7.1, 0]) // eyeballed based on mounting posts
		clearance_layout(layout = planck);
	};
};

translate([plate_x, 0, 0])
rotate([0, 180, 0])
switched_mounted_plate();
